# generalized manipop

Codes for the paper J. Tachella, Y. Altmann, S. McLaughlin and J. -Y. Tourneret, "3D Reconstruction Using Single-photon Lidar Data Exploiting the Widths of the Returns," ICASSP 2019 - 2019 IEEE International Conference on Acoustics, Speech and Signal 